<div class="container">
	<div class="row mt-3 justify-content-center">
		<div class="col-4">
			<div class="card text-center">
				<div class="card-header">
					Silakan Register
				</div>
				<div class="card-body">
					<form action="<?= BASEURL; ?>/register/simpanUser" method="post">
						<div class="mb-3">
							<label for="luser" class="form-label">Username</label>
							<input type="text" class="form-control" id="iduser_reg" name="tuser_reg" required>
						</div>
						<div class="mb-3">
							<label for="lpass" class="form-label">Password</label>
							<input type="password" class="form-control" id="password_reg" name="tpass_reg" required>
						</div>
						<div class="mb-3">
							<label for="lpass" class="form-label">Konfirmasi Password</label>
							<input type="password" class="form-control" id="konf_password" name="tpass_reg_konf" required>
						</div>
						<div class="mb-3">
							<button class="btn btn-primary" type="submit">Register</button>
						</div>
					</form>
				</div>
				<div class="card-footer text-muted">
					<p class="card-text">Sudah Punya Akun? <a href="<?= BASEURL; ?>/home">Login Disini</a></p>
				</div>

			</div>
		</div>
	</div>
</div>