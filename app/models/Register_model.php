<?php
class Register_model
{

    private $table = 'user';
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    public function tambahUser($username, $password, $iduser)
    {
        $this->db->query('SELECT (max(idUser)+1) as maks_id  FROM ' . $this->table);
        $data = $this->db->resultSet();
        // foreach ($data as $rec) {
        //     $id = $rec["maks_id"];
        // }
        $this->db->query('INSERT INTO ' . $this->table . '(iduser,username, password) VALUES(:idUser,:username, :password)');
        $this->db->bind('idUser', $iduser);
        $this->db->bind('username', $username);
        $this->db->bind('password', $password);
        $this->db->execute();
    }
}
