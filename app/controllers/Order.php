<?php

class Order extends Controller
{

   public function index()
   {
      session_start();
      if (isset($_SESSION["iduser"])) {
         $data['title'] = 'Data Order';
         $data['order'] = $this->model('Order_model')->getAllOrder();
         $this->view('templates/header');
         $this->view('order/index', $data);
         $this->view('templates/footer');
      } else {
         $this->view('templates/header2');
         $this->view('user/login');
         $this->view('templates/footer');
      }
   }

   // public function delorder($id)
   // {
   //    $this->model('Order_model')->del($id);
   //    header("location:" . BASEURL . "/order");
   // }
}
