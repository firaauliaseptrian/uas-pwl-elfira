<?php
class Register extends Controller
{

    public function index()
    {
        $data['title'] = 'Registrasi';
        $this->view('templates/header2');
        $this->view('register/index');
        $this->view('templates/footer');
    }
    public function simpanUser()
    {
        $password    = $_POST['tpass_reg_konf'];
        $username = $_POST['tuser_reg'];
        $iduser = $_POST['iduser'];

        $data['usr'] = $this->model('Register_model')->tambahUser($username, $password, $iduser);
        header('location:../register');
    }
}
